package com.choucair.formacion.pageobjects;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")
public class RegistroPacientePageObject extends PageObject {

	public void ingresopaciente(String nombre, String apellido, String telefono, String numeroID) {
	
		findBy("//*[@id=\"page-wrapper\"]/div/div[2]/div/div/div/div/div[1]/div/a[2]").click();
		findBy("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[1]/input").type(nombre); 
		findBy("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[2]/input").type(apellido);
		findBy("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[3]/input").type(telefono);
		findBy("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[4]/select/option[1]").click(); 
		findBy("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[5]/input").type(numeroID);
		findBy("//div[@class='checkbox']//label[1]").click();
		findBy("//*[@id=\"page-wrapper\"]/div/div[3]/div/a").click();
		waitABit(6000);
		
	}


}
