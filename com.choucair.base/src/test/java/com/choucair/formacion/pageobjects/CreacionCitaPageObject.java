package com.choucair.formacion.pageobjects;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")
public class CreacionCitaPageObject extends PageObject {

	public void creacioncita(String diacita,String docpaciente, String docdoctor, String observacion) {
		// TODO Auto-generated method stub

		findBy("//*[@id=\"page-wrapper\"]/div/div[2]/div/div/div/div/div[1]/div/a[6]").click();
		findBy("//*[@id=\"datepicker\"]").type(diacita);
		findBy("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[3]/td[4]/a").click();
		findBy("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[2]/input").type(docpaciente);
		findBy("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[3]/input").type(docdoctor);
		findBy("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[4]/textarea").click();
		findBy("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[4]/textarea").type(observacion);
		findBy("//*[@id=\"page-wrapper\"]/div/div[3]/div/a").click();
		waitABit(6000);
	}

}
