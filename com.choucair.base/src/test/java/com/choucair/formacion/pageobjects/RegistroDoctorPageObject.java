package com.choucair.formacion.pageobjects;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")
public class RegistroDoctorPageObject extends PageObject {

	
	public void ingresoDoctor(String nombre, String apellido, String telefono, String numeroID) {

		findBy("//*[@id=\"page-wrapper\"]/div/div[2]/div/div/div/div/div[1]/div/a[1]").click();
		findBy("//*[@id=\'name\']").type(nombre);
		findBy("//*[@id=\'last_name\']").type(apellido);
		findBy("//*[@id=\'telephone\']").type(telefono);
		findBy("//select[@class='form-control']/option[1]").click(); 
		findBy("//*[@id=\'identification\']").type(numeroID);
		findBy("//a[@class='btn btn-primary pull-right']").click();
		waitABit(6000);
		
	}

	public void registroexitoso() {
		// TODO Auto-generated method stub

	}

}
