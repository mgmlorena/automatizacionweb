package com.choucair.formacion.definition;

import com.choucair.formacion.steps.CreacionCitaSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class CreacionCitaDefinition {

	@Steps
	CreacionCitaSteps CreacioncitaSteps;
	
	@Given("^Dado que Carlos necesita asistir al medico$")
	public void dado_que_Carlos_necesita_asistir_al_medico() throws Throwable {
		CreacioncitaSteps.AbrirUrl();
	}

	@When("^Cuando el realiza el agendamiento de una Cita$")
	public void cuando_el_realiza_el_agendamiento_de_una_Cita() throws Throwable {
		CreacioncitaSteps.Agendarcita();
	}

	@Then("^Verifica que se presente en pantalla el mensaje Datos guardados correctamente$")
	public void verifica_que_se_presente_en_pantalla_el_mensaje_Datos_guardados_correctamente() throws Throwable {
		CreacioncitaSteps.RegistroExitoso();
	}

}
