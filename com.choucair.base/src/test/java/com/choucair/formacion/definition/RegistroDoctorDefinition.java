package com.choucair.formacion.definition;

import com.choucair.formacion.steps.RegistroDoctorSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class RegistroDoctorDefinition {
	
	@Steps 
	RegistroDoctorSteps IngresoCreacionCitaMedicaSteps;

	@Given("^Que Carlos necesita registrar un nuevo doctor$")
	public void que_Carlos_necesita_registrar_un_nuevo_doctor() throws Throwable {
		IngresoCreacionCitaMedicaSteps.AbrirUrlAdminHospitales();
	
	}


	@When("^El realiza el registro del mismo en el aplicativo de Administración de Hospitales$")
	public void el_realiza_el_registro_del_mismo_en_el_aplicativo_de_Administración_de_Hospitales() throws Throwable {
		IngresoCreacionCitaMedicaSteps.RegistroDoctor();
	  
	}

	@Then("^El verifica que se presente en pantalla el mensaje Datos guardados correctamente$")
	public void el_verifica_que_se_presente_en_pantalla_el_mensaje_Datos_guardados_correctamente() throws Throwable {
		IngresoCreacionCitaMedicaSteps.RegistroExitoso();
	 
	}
	

}
