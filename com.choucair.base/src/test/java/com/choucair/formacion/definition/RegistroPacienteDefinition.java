package com.choucair.formacion.definition;

import com.choucair.formacion.steps.RegistroPacienteSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class RegistroPacienteDefinition {

	@Steps 
	RegistroPacienteSteps IngresoRegistroPacienteSteps;
	
	@Given("^Dado que Carlos necesita registrar un nuevo paciente$")
	public void dado_que_Carlos_necesita_registrar_un_nuevo_paciente()throws Throwable {
		IngresoRegistroPacienteSteps.AbrirUrlRegistroPaciente();
	}

	@When("^Cuando el realiza el registro del mismo en el aplicativo de Administración de Hospitales$")
	public void cuando_el_realiza_el_registro_del_mismo_en_el_aplicativo_de_Administración_de_Hospitales() throws Throwable {
		IngresoRegistroPacienteSteps.RegistroPaciente();
	}

	@Then("^Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente$")
	public void entonces_el_verifica_que_se_presente_en_pantalla_el_mensaje_Datos_guardados_correctamente() throws Throwable {
		IngresoRegistroPacienteSteps.RegistroExitoso();
	}


	
	
}
