package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.RegistroDoctorPageObject;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class RegistroDoctorSteps {

	RegistroDoctorPageObject IngresoCreacionCitaMedicaPageObject;

	@Step
	public void AbrirUrlAdminHospitales() {
		// TODO Auto-generated method stub
		IngresoCreacionCitaMedicaPageObject.open();

	}

	@Step
	public void RegistroDoctor() {
		String nombre = "pedro";
		String apellido = "lopez";
		String telefono = "2121212";
		String numeroID = "123456";

		IngresoCreacionCitaMedicaPageObject.ingresoDoctor(nombre, apellido, telefono, numeroID);
	}

	@Step
	public void RegistroExitoso() {
		// TODO Auto-generated method stub
		IngresoCreacionCitaMedicaPageObject.registroexitoso();
	}

}
