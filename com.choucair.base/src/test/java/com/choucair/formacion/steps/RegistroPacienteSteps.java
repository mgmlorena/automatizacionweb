package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.RegistroPacientePageObject;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class RegistroPacienteSteps {

	RegistroPacientePageObject IngresoCreacionPacientePageObject;
	
	@Step
	public void AbrirUrlRegistroPaciente() {
		IngresoCreacionPacientePageObject.open();
		
	}

	public void RegistroPaciente() {
		String nombre = "Lorena";
		String apellido = "Perez";
		String telefono = "31155152";
		String numeroID = "789123";

		IngresoCreacionPacientePageObject.ingresopaciente(nombre, apellido, telefono, numeroID);
	}

	public void RegistroExitoso() {
		// TODO Auto-generated method stub
		
	}

}
